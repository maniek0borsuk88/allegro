<?php
/**
 * Created by PhpStorm.
 * User: Maniek
 * Date: 2018-09-08
 * Time: 10:29
 */

namespace allegro;


class Controller {

	public function render($view) {

		require_once PUBLIC_PATH . $view;
	}

}