<?php
/**
 * Validation
 */
namespace allegro\core;


use allegro\db\DB_Connect;

class Validation extends DB_Connect {

	/**
	 *
	 * Validation Email
	 *
	 * @param $email - email
	 * @param $error - Error full name
	 */
	public static function emailValidation($email, $error) {

		if(!preg_match("/^[a-zA-Z0-9_.]{2,35}@[a-zA-Z0-9_.]{2,30}$/",$email)) {

			echo $error;
			$email = false;
			return $email;

		} else {

			return $email;

		}

	}


	/**
	 *
	 * Method verify login and email
	 *
	 * @param $email
	 * @param $error
	 *
	 */
	public static function verifyLoginEmail($email,$error) {

		$emailPreg = !preg_match("/^[a-zA-Z0-9_.]{2,35}@[a-zA-Z0-9_.]{2,30}$/",$email);
		$loginPreg = !preg_match('@^[a-zA-Z0-9\@#$%&!+-_=]{2,30}$@', $email);

		if($emailPreg && $loginPreg) {

			echo $error;
			$email = false;
			return $email;

		} else {

			return $email;

		}

	}

	/**
	 *
	 *  Validation Text String
	 *
	 * @param $string - text in input
	 * @param $error - Error full name
	 */
	public static function stringTextValidation($string, $error) {

		if(!preg_match("/^[a-zA-Z]{2,35}$/",$string)) {

			echo $error;
			$string = false;
			return $string;

		} else {

			return $string;

		}

	}

	/**
	 *
	 *  Validation Text String Poland
	 *
	 * @param $string - text in input
	 * @param $error - Error full name
	 */
	public static function stringPoland($string, $error) {

		if(!preg_match("/^[a-zA-ZąęćżóńłśĄĘĆŻÓŃŁŚ\s]{2,35}$/",$string)) {

			echo $error;
			$string = false;
			return $string;

		} else {

			return $string;

		}

	}


	/**
	 *
	 * Method Validation text
	 *
	 * @param $text
	 * @param $error
	 */
	public static function textValidation($text,$error) {

		if(!preg_match('@^[a-zA-ZąęćżóńłśĄĘĆŻÓŃŁŚ\s0-9\@.,/]{5,255}$@',$text)) {

			echo $error;
			$text = false;
			return $text;

		} else {

			return $text;

		}

	}


	/**
	 *
	 *  Validation Login
	 *
	 * @param $login
	 * @param $error
	 */
	public static function loginValidation($login, $error) {

		if(!preg_match('@^[a-zA-Z0-9\@#$%&!+-_=]{2,30}$@', $login)) {

			echo $error;
			$login = false;
			return $login;

		} else {

			return $login;

		}

	}


	/**
	 *
	 * Method Validate Telephone Number Kom and Normal + prefix(kod)
	 *
	 * @param $tel
	 * @param $error
	 *
	 * @return false or telephone
	 */
	public static function validateTel($tel,$error) {

		/* Example 500500500 and 297414788  tel kom and sta + kod = 9 */
		$telKom1 = !preg_match('@^[0-9]{7,13}$@', $tel);

		/* Example 500-500-500 */
		$telKom2 = !preg_match('@^[0-9]{3}-[0-9]{3}-[0-9]{3}$@',$tel);

		/* Example 500 500 500 */
		$telKom3 = !preg_match('@^[0-9]{3} [0-9]{3} [0-9]{3}$@',$tel);

		/* Example  29-74-000-00 */
		$telSt = !preg_match('@^[0-9]{2}-[0-9]{2}-[0-9]{3}-[0-9]{2}$@',$tel);

		/* Example 29 00 000 00 */
		$telSt2 = !preg_match('@^[0-9]{2} [0-9]{2} [0-9]{3} [0-9]{2}$@',$tel);

		if( $telKom1 && $telKom2 && $telKom3 && $telSt && $telSt2) {

			echo $error;
			$tel = false;
			return $tel;

		} else {

			return $tel;

		}

	}

	/**
	 * Validate Poland Nip
	 *
	 * @param $nip
	 * @param $error
	 */
	public static function nipValidate($nip,$error) {

		/* Example 3330002244 */
		$nip1 = !preg_match('@^[0-9]{10}$@',$nip);

		/* Example 333-000-22-44 */
		$nip2 = !preg_match('@^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$@',$nip);

		/* Example 333 000 22 44 */
		$nip3 = !preg_match('@^[0-9]{3} [0-9]{3} [0-9]{2} [0-9]{2}$@',$nip);

		if($nip1 && $nip2 && $nip3) {

			echo $error;
			$nip = false;
			return $nip;

		} else {

			return $nip;

		}
	}

	/**
	 *  Method Validate Code Post in Poland
	 *
	 * @param $kod
	 * @param $error
	 *
	 * @return $kod or false
	 */
	public static function codePost($kod,$error) {

		if(!preg_match('@^[0-9]{2}-[0-9]{3}$@',$kod)) {

			echo $error;
			$kod = false;
			return $kod;

		} else {

			return $kod;

		}

	}


	/**
	 *
	 * Validation Password
	 *
	 * @param $password - password text input
	 * @param $error - Error full name
	 */
	public static function validPassword($password, $error) {

		if(!preg_match("/^[a-zA-ZąęćżóńłśĄĘĆŻÓŃŁŚ0-9.-_=+%$#@!]{7,16}$/", $password) && !preg_match('/[A-ZĄĆĘŁŃÓŚŹŻ]/', $password)) {

				echo $error;
				$password = false;
				return $password;


		} else {

			return $password;

		}


	}


	/**
	 *
	 * Validation Date
	 *
	 * @param $date - date
	 * @param $error - full error name
	 */
	public static function validDate($date, $error) {

		$dateArray = explode('-', $date);

		$day = (int)$dateArray[2];
		$month = (int)$dateArray[1];
		$year = (int)$dateArray[0];

		if($month < 9) {

			$month = 0 . $month;

		}

		/* Verification month, day and year for function checkdate */
		if(checkdate($month , $day , $year)) {

			$date =  $year . '-' . $month . '-' . $day;
			return $date;

		} else {

			echo $error;

		}

	}


	public static function checkDate($date,$error){

		$dateArray = explode('-', $date);

		$day = (int)$dateArray[2];
		$month = (int)$dateArray[1];
		$year = (int)$dateArray[0];

		if(checkdate($month , $day , $year)) {

			$date =  $year . '-' . $month . '-' . $day;
			return $date;

		} else {

			echo $error;
			$date = false;
			return $date;

		}

	}



	/**
	 * @param $year
	 *
	 * Verifycation Year User
	 */
	public static function yearCheck($year) {

		$years = date('Y') - 14;
		$yearsOld = date('Y') - 110;

		if($year >= $yearsOld && $year <= $years) {

			return $year;

		} else {

			echo '<p class="text-danger">Nie poprawny format roku!<small> <br>Rok powinien mieścić się w przedziale '
			     . $yearsOld . ' do ' . $years . '.Musisz mieć ukończone 14 lat by móc korzystać z naszego serwisu.</small>';
			$year = false;
			return $year;

		}

	}


	/**
	 *
	 * Veryfication passwords
	 *
	 * @param $pass1 - password verify 1
	 * @param $pass2 - password verify 2
	 * @param $error - full name error
	 */
	public function verificationPasswords($pass1, $pass2, $error) {

		if($pass1 !== $pass2) {

			echo $error;

		} else {

			return TRUE;

		}


	}


	/**
	 *
	 * Verify Email repeated
	 *
	 * @param $db_name
	 * @param $email
	 * @param $val
	 *
	 * @return bool
	 */
	public function emailVerifyDb($db_name, $email, $val) {

		$query = "SELECT * FROM `$db_name`";

		$result = $this->db->query($query);
		$result->execute();
		$key = $result->fetchAll();

		foreach ($key as $value) {

			$emvery= $value[$val];

		}

		if($email !== $emvery) {

			return $email;

		} else {

			$email = FALSE;
			echo 'Podany adres email istnieje w bazie';

		}

	}


	/**
	 *
	 * Weryfikuje dwa parametry czy sa takie same
	 *
	 * @param $key1
	 * @param $key2
	 *
	 * @return bool
	 */
	public function searchRepeatedVal($key1,$key2) {

		if($key1 != $key2) {

			$key = false;
			return $key;

		} else {

			$key = true;
			return $key;

		}

	}


	/**
	 * Verify price and return price ok format
	 *
	 * @param $price
	 * @param $error
	 *
	 * @return string
	 */
	public function priceNumber($price, $error) {

		if (is_numeric($price)) {

			$price = number_format($price,2,'.' , ' ');

			return $price;

		} else {

			return $error;

		}

	}


	/**
	 * @param $check
	 * @param $val
	 * @param $error
	 *
	 * Check accept checkbox
	 */
	public static function acceptCheckBox($check, $val, $error) {

		if($check === $val) {

			return $check;

		} else {

			echo $error;
			unset($check);

		}

	}

	/**
	 * @param $check
	 * @param $val
	 *
	 * @return int 1 or 0 (true or false)
	 */
	public static function acceptCheckbox2($check,$val) {

		if($check === $val) {

			$check = 1;
			return $check;

		} else {

			$check = 0;
			return $check;

		}

	}


	/**
	 * @param $post
	 *
	 * @ Method is check $_POST is empty
	 */
	public static function emptyPost($post) {

		if(!empty($post)) {

			return $post;

		} else {

			return false;

		}

	}


	/**
	 * @param $day
	 * @param $error
	 *
	 * @return true = $day provided by user
	 * @return false = $error day in month
	 */
	public static function validDayDate($day,$error) {

		$day_month = date('t');

		if($day >=1 && $day <= $day_month) {

			return $day;

		} else {

			echo $error;
			$day = false;
			echo $day;

		}

	}


	/**
	 * @param $val
	 *
	 * Method change string month on number
	 */
	public static function monthChangeNumber($val) {

		$month = [

			'styczen' => '01',
			'luty' => '02',
			'marzec' => '03',
			'kwiecien' => '04',
			'maj' => '05',
			'czerwiec' => '06',
			'lipiec' => '07',
			'sierpien' => '08',
			'wrzesien' => '09',
			'pazdziernik' => '10',
			'listopad' => '11',
			'grudzien' => '12'

		];

		return $month[$val];

	}


}