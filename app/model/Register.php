<?php


namespace allegro\model;


use allegro\assets\ErrorCode;
use allegro\core\Validation;

class Register extends Validation {

	protected $email;
	protected $password;
	protected $day_birth;
	protected $month_birth;
	protected $year_birth;
	protected $date_birth;
	protected $accept_reg;
	protected $accept_han;
	protected $accept_buss;
	protected $date_register;
	protected $login;
	protected $telephone;
	protected $country;
	protected $info_buss;
	protected $nip;
	protected $address;
	protected $codePost;
	protected $townCodePost;


	/**
	 *  Method Checking the form type
	 */
	public function registerSearch() {

		if(isset($_POST['reg_simple'])) {

			$this->emptySampleFormReg();

		} elseif(isset($_POST['reg_firm'])) {

			$this->registerBusinnessUser();

		}

	}

	/**
	 * Method Validate Form Sample Register and Save User in Data Base
	 */
	public function emptySampleFormReg() {

		if(!empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['day_birth']) && !empty($_POST['month_birth']) && !empty($_POST['year_birth']) && isset($_POST['regulamin_accept'])) {

			/* Validation Email */
			$this->email = Validation::emailValidation($_POST['email'], ErrorCode::errorCode('email'));
			($this->email !== false) ? $email = htmlentities($this->email,ENT_QUOTES) : $email = false;

			/* Validation Password */
			$this->password = Validation::validPassword($_POST['password'],ErrorCode::errorCode('password'));
			($this->password !== false) ? $password = htmlentities($this->password, ENT_QUOTES) : $password = false;

			/* Validation Day Birth */
			$this->day_birth = Validation::validDayDate($_POST['day_birth'],ErrorCode::errorCode('day'));
			($this->day_birth !== false) ? $day_birth = htmlentities($this->day_birth, ENT_QUOTES) : $day_birth = false;

			/* Validation Month Birth */
			$this->month_birth = Validation::monthChangeNumber($_POST['month_birth']);
			 $month_birth = htmlentities($this->month_birth, ENT_QUOTES);

			 /* Validation Year Birth */
			$this->year_birth = Validation::yearCheck($_POST['year_birth']);
			($this->year_birth !== false) ? $year_birth = htmlentities($this->year_birth, ENT_QUOTES) : $year_birth = false;

			/* Validation All Date */
			$this->date_birth = $year_birth . '-' . $month_birth . '-' . $day_birth;
			$this->date_birth = Validation::checkDate($this->date_birth,ErrorCode::errorCode('date'));
			($this->date_birth !== false) ? $date_birth = htmlentities($this->date_birth, ENT_QUOTES) : $date_birth = false;

			/* Validation Regulamin Accept */
			$this->accept_reg = Validation::acceptCheckBox($_POST['regulamin_accept'], 'accept', ErrorCode::errorCode('accept_reg'));

			/* Check information provisions trade */
			$accept_han = $this->checkInfoTrade();

			/* Check information info trade business */
			$accept_buss = $this->checkInfoBus();

			/* Date Register */
			$this->date_register =  date('Y-m-d G:i:s');
			$date_register = $this->date_register;

			/* Type Account */
			$type_account = 'normal';



			/*  If !== false  // operator AND && */
			/* If === false  // operator || OR  */
			if($email === false || $password === false || $day_birth === false || $year_birth === false || $date_birth === false) {

				echo '<p class="text-danger alert-danger p-3 border-danger">Proszę o wpisanie poprawnie danych</p>';

			} else {

				/* Query Check E-mail from Data Base */
				$query = "SELECT email FROM users WHERE email=:email UNION SELECT email FROM business_account WHERE email=:email";
				$result = $this->db->prepare($query);
				$result->bindValue(':email',$email,\PDO::PARAM_STR);
				$result->execute();
				$row = $result->rowCount();

				if($row > 0) {

					echo '<p class="alert-danger text-danger p-3 border-danger">Użytkownik o podanym adresie e-mail istnieje w serwisie!</p>';

				} else {

					$password = password_hash($password, PASSWORD_BCRYPT);

					$query = "INSERT INTO users(id,email,password,date_birth,date_register,type_account,info_handlowe,info_firm) VALUES(null,:email,:password,:date_birth,:date_register,:type_account,:info_han,:info_buss)";
					$row = $this->db->prepare($query);
					$row->bindValue(':email',$email, \PDO::PARAM_STR);
					$row->bindValue(':password',$password, \PDO::PARAM_STR);
					$row->bindValue(':date_birth',$date_birth,\PDO::PARAM_STR);
					$row->bindValue(':date_register',$date_register,\PDO::PARAM_STR);
					$row->bindValue('type_account',$type_account, \PDO::PARAM_STR);
					$row->bindValue(':info_han',$accept_han,\PDO::PARAM_INT);
					$row->bindValue(':info_buss',$accept_buss, \PDO::PARAM_INT);
					$user = $row->execute();

					echo '<p class="alert-success text-success p-3 border-success">Pomyślne zapisano ' . $user . ' użytkownika.</p>';

				}

			}


		} else {

			echo 'Wszystkie pola muszą być wypełnione!';

		}

	}


	/**
	 * Method Validate Form Business Register and Save User in Data Base
	 */
	public function registerBusinnessUser() {

		if(!empty($_POST['email'])  && !empty($_POST['login']) && !empty($_POST['password']) && !empty($_POST['telephone']) && !empty($_POST['country']) && !empty($_POST['info_firma']) && !empty($_POST['nip']) && !empty($_POST['address']) && !empty($_POST['kod']) && isset($_POST['regulamin_accept']) && !empty($_POST['miasto']))  {

			/* Validation Email */
			$this->email = Validation::emailValidation($_POST['email'], ErrorCode::errorCode('email'));
			($this->email !== false) ? $email = htmlentities($this->email,ENT_QUOTES) : $email = false;

			/* Validation Login */
			$this->login = Validation::loginValidation($_POST['login'],ErrorCode::errorCode('login'));
			($this->login !== false) ? $login = htmlentities($this->login,ENT_QUOTES) : $login = $this->login;

			/* Validation Password */
			$this->password = Validation::validPassword($_POST['password'],ErrorCode::errorCode('password'));
			($this->password !== false) ? $password = htmlentities($this->password, ENT_QUOTES) : $password = false;

			/* Validation Telephone Number */
			$this->telephone = Validation::validateTel($_POST['telephone'], ErrorCode::errorCode('telephone'));
			($this->telephone !== false) ? $tel = htmlentities($this->telephone, ENT_QUOTES) : $tel = false;

			/* Validation Country */
			$this->country = Validation::stringTextValidation($_POST['country'], ErrorCode::errorCode('country'));
			($this->country !== false) ? $country = htmlentities($this->country,ENT_QUOTES) : $country = false;

			/* Validation Business info */
			$this->info_buss = Validation::textValidation($_POST['info_firma'],ErrorCode::errorCode('description'));
			($this->info_buss !== false) ? $info_buss = htmlentities($this->info_buss,ENT_QUOTES) : $info_buss = false;

			/* Validation NIP */
			$this->nip = Validation::nipValidate($_POST['nip'],ErrorCode::errorCode('nip'));
			($this->nip !== false) ? $nip = htmlentities($this->nip,ENT_QUOTES) : $nip = false;

			/* Validation Address */
			$this->address = Validation::textValidation($_POST['address'],ErrorCode::errorCode('address'));
			($this->address !== false) ? $address = htmlentities($this->address,ENT_QUOTES) : $address = false;

			/* Validation Code Post*/
			$this->codePost = Validation::codePost($_POST['kod'],ErrorCode::errorCode('code_post'));
			($this->codePost !== false) ? $codePost = htmlentities($this->codePost,ENT_QUOTES) : $codePost = false;

			/* Validation Name City, Town for code post */
			$this->townCodePost = Validation::stringPoland($_POST['miasto'],ErrorCode::errorCode('town'));
			($this->townCodePost !== false) ? $townCodePost = htmlentities($this->townCodePost,ENT_QUOTES) : $townCodePost = false;

			/* Validation Regulamin Accept */
			$this->accept_reg = Validation::acceptCheckBox($_POST['regulamin_accept'], 'accept', ErrorCode::errorCode('accept_reg'));

			/* Check information provisions trade */
			$accept_han = $this->checkInfoTrade();

			/* Check information info trade business */
			$accept_buss = $this->checkInfoBus();

			/* Date Register */
			$this->date_register =  date('Y-m-d G:i:s');
			$date_register = $this->date_register;

			/* Type Account */
			$type_account = 'business';

			/*  If !== false  // operator AND && */
			/* If === false  // operator || OR  */
			if($email !== false && $login !== false && $password !== false && $tel !== false && $country !== false && $info_buss !== false && $nip !== false && $address !== false && $townCodePost !== false) {

				$query = "SELECT users.email FROM users WHERE users.email=:email UNION SELECT business_account.email FROM business_account WHERE business_account.email=:email";
				$result = $this->db->prepare($query);
				$result->bindValue(':email',$email,\PDO::PARAM_STR);
				$result->execute();
				$row = $result->rowCount();

				if($row > 0) {

					echo '<p class="alert-danger text-danger p-3 border-danger">Istnieje konto o podanym loginie lub adresie e-mail.</p>';

				} else {

						try {

							$password = password_hash($password,PASSWORD_BCRYPT);

							$query = "INSERT INTO business_account(id,email,login,password,telephone,country,info_buss,nip,address,code_post,town_code,accept_han,accept_buss,type_account,date_register) VALUES(null,:email,:login,:password,:telephone,:country,:info_buss,:nip,:address,:code_post,:town_code,:accept_han,:accept_buss,:type_account,:date_register)";

							$result = $this->db->prepare($query);
							$result->bindValue(':email',$email,\PDO::PARAM_STR);
							$result->bindValue(':login',$login,\PDO::PARAM_STR);
							$result->bindValue(':password',$password,\PDO::PARAM_STR);
							$result->bindValue(':telephone',$tel,\PDO::PARAM_STR);
							$result->bindValue(':country',$country,\PDO::PARAM_STR);
							$result->bindValue(':info_buss',$info_buss,\PDO::PARAM_STR);
							$result->bindValue(':nip',$nip,\PDO::PARAM_STR);
							$result->bindValue(':address',$address,\PDO::PARAM_STR);
							$result->bindValue(':code_post',$codePost,\PDO::PARAM_STR);
							$result->bindValue(':town_code',$townCodePost,\PDO::PARAM_STR);
							$result->bindValue(':accept_han',$accept_han,\PDO::PARAM_INT);
							$result->bindValue(':accept_buss',$accept_buss,\PDO::PARAM_INT);
							$result->bindValue(':type_account',$type_account,\PDO::PARAM_STR);
							$result->bindValue(':date_register',$date_register,\PDO::PARAM_STR);
							$row = $result->execute();

							echo '<p class="alert-success text-success p-3 border-success">Zapisano ' . $row . ' nowe konto!</p>';

						} catch (\Exception $e) {

							echo $e->getMessage();

						}

				}

			} else {

				echo '<p class="alert-danger text-danger p-3 border-danger">Proszę o wypełnienie poprawnie wszystkich danych!</p>';

			}



		} else {

			echo 'Proszę o wypełnienie wszystkich pól.';

		}

	}


	/**
	 *
	 * Method check $_POST info triade is value "accept"
	 *
	 * @return int 1 or 0
	 */
	public function checkInfoTrade() {

		$accept_han = 0;

		if(isset($_POST['postanowienia_accept'])) {
			$this->accept_han = Validation::acceptCheckbox2($_POST['postanowienia_accept'],'accept');
			$accept_han = $this->accept_han;

			return $accept_han;
		}

		return $accept_han;

	}


	/**
	 *
	 * Method check $_POST info business is value "accept"
	 *
	 * @return int 1 or 0
	 */
	public function checkInfoBus() {

		$accept_buss = 0;

		if(isset($_POST['handlowe_info'])) {
			$this->accept_buss = Validation::acceptCheckbox2($_POST['handlowe_info'],'accept');
			$accept_buss = $this->accept_buss;

			return $accept_buss;
		}

		return $accept_buss;

	}


}