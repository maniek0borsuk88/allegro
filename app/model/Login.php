<?php
/**
 * Class Login
 */

namespace allegro\model;


use allegro\assets\ErrorCode;
use allegro\core\Validation;

class Login extends Validation {

	protected $email;
	protected $password;

	public function __construct( $db = null ) {
		parent::__construct( $db );

		if(isset($_POST['submit_login'])) {

			echo 'Kliknieto submit';
			if(!empty($_POST['email']) && !empty($_POST['password'])) {

				/* Validation Login */
				$this->email = Validation::verifyLoginEmail($_POST['email'],ErrorCode::errorCode('email'));
				($this->email !== false) ? $email = htmlentities($this->email,ENT_QUOTES) : $email = false;

				/* Validation password */
				$this->password = Validation::validPassword($_POST['password'], ErrorCode::errorCode('password'));
				($this->password !== false) ? $password = htmlentities($_POST['password'],ENT_QUOTES) : $password = false;

				if($email !== false && $password !== false) {

					/*************************************
					*
					 *
					 *  Nie zapomnij o zbindowaniu i sprawdzeniu
					 * poprawnego działania
					 * wartośći wprowadzanych przez użytkoenika => $email
					 *
					 *
					 *
					*************************/


					$query = "SELECT password,email,type_account FROM users WHERE users.email='$email' UNION SELECT password,email,type_account FROM business_account WHERE business_account.email='$email' OR business_account.login='$email'";
					$val = $this->db->query($query);
					$val->execute();
					$user= $val->rowCount();
					$account = $val->fetchAll();
					if($user > 0) {

						$passwordHash = $user['password'];

						$password = password_verify($password,$passwordHash);

						if($user > 0 && $password !== 0){

							$_SESSION['user'] = $account;

							if($_SESSION['user']) {

								header('Location: /allegro/myaccount/mojekonto');

							}

						} else {

							echo '<p class="alert-danger text-danger p-3 border-danger">Brak użytkownika o podanym loginie lub adresie e-mail</p>';

						}

					}

				}

			}

		} else {

			echo 'Nie kliknięto przycisku Submit';

		}


	}


}