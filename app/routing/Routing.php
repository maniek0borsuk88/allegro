<?php
namespace allegro\routing;


class Routing {

	protected $namespace = 'allegro\\controller\\';
	protected $controller = 'home';
	protected $method = 'index';
	protected $params = [];

	public function __construct() {

		$this->parseUrl();

	}

	public function parseUrl() {

		$url = isset($_GET['url']) ? $url = $_GET['url'] : 'home';

		$this->controller = explode('/', $url);

		if(file_exists(APP_PATH . 'controller' . DS . $this->controller[0] . 'Controller.php' )) {

			$control = $this->namespace . $this->controller[0] . 'Controller';
			$control = new $control;
			unset($this->controller[0]);


			if(isset($this->controller[1])) {

				if (method_exists($control, $this->controller[1])) {

					$this->method = $this->controller[1];
					call_user_func_array([$control,$this->method],$this->params);

				} else {

					include PUBLIC_PATH . '404.php';

				}

			}

		} else {

			include PUBLIC_PATH . '404.php';

		}

	}

}