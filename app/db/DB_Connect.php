<?php
/**
 * DB_Connect
 */

namespace allegro\db;


class DB_Connect {

	protected $db;

	public function __construct($db=null) {

		$dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";encoding=utf8";

		try {

			$this->db = new \PDO($dsn, DB_USER,DB_PASS);
			$this->db->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE,\PDO::FETCH_ASSOC);

		} catch(\Exception $e) {

			die($e->getMessage());

		}

	}

}