<?php
/**
 * Created by PhpStorm.
 * User: Maniek
 * Date: 2018-09-08
 * Time: 12:56
 */

namespace allegro\controller;


use allegro\Controller;

class DzialController extends Controller {

	/**
	 * Metoda kierująca na widok do działu z Motoryzacją
	 */
	public function moto() {

		$this->render('dzial\motoryzacja.php');

	}

	/**
	 * Metoda kierująca na widok do działu z Elektroniką
	 */
	public function electro() {

		$this->render('dzial\elektronika.php');

	}

	/**
	 * Metoda kierująca na widok do działu z Modą
	 */
	public function moda() {

		$this->render('dzial\moda.php');

	}

	/**
	 * Metoda kierująca na widok do działu z Nieruchomościami
	 */
	public function nieruchomosc() {

		$this->render('dzial\nieruchomosc.php');

	}

	/**
	 * Metoda kierująca na widok do działu Sportowego
	 */
	public function sport() {

		$this->render('dzial\sport.php');

	}

}