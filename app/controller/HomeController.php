<?php
namespace allegro\controller;

use allegro\Controller;

class HomeController extends Controller {

	public function __construct() {

		$this->render('index.php');

	}

}