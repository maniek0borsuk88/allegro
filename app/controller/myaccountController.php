<?php

namespace allegro\controller;


use allegro\Controller;

class myaccountController extends Controller {

	public function addproduct() {

		$this->render('myaccount\addproduct.php');

	}

	public function kupione() {

		$this->render('myaccount\kupione.php');

	}

	public function ulubione() {

		$this->render('myaccount\ulubione.php');

	}

	public function mojeaukcje() {

		$this->render('myaccount\mojeaukcje.php');

	}

	public function mojekonto() {

		$this->render('myaccount\mojekonto.php');

	}

	public function logOut() {

		$this->render('myaccount\logout.php');

	}

}