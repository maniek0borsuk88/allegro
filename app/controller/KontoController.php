<?php

namespace allegro\controller;


use allegro\Controller;

class KontoController extends Controller {

	public function login() {

		$this->render('konto\login.php');

	}

	public function register() {

		$this->render('konto\register.php');

	}

	public function firmowe() {

		$this->render('konto\konto-firmowe.php');

	}

}