<?php
/**
 * Error Code Class
 */

namespace allegro\assets;


class ErrorCode {

	public static function errorCode($val) {

		$error = [

			'email' => '** Proszę wpisać poprawny adres e-mail.',
			'password' => '** Hasło powinno zawierać: jedną wielką literę, jedną małą literę, jedną cyfrę, od 8 do 16 znaków.',
			'date' => '** Proszę podac poprawny format daty.',
			'day' => '** Proszę o wpisanie poprawnego dnia urodzin.',
			'accept_reg' => '** Proszę zaakceptować regulamin.',
			'login' => '** Login musi się składać od 2 do 20 znaków, dużych, małych liter,znaków specjalnych:@#$%&!+-_= , cyfr, nie może zawierać polskich znaków!',
			'telephone' => '** Proszę podać poprawny numer telefonu!',
			'country' => '** Prosze wpisać poprawnie kraj.',
			'description' => '** Proszę o opisanie poprawnie firmy!',
			'nip' => '** Nie poprawny NIP!',
			'address' => '** Nie poprawny Adres!',
			'code_post' => '** Nie poprawny format kodu pocztowego.',
			'town' => '** Prosze wpisać poprawnie miejscowość kodu pocztowego',
			'log_email' => '** Niepoprawny login lub hasło.'


		];

		return '<p class="text-danger">' . $error[$val] . '</p>';

	}


}