<?php include_once PUBLIC_PATH . 'header-empty.php'; ?>

<div id="register">
    <div class="login-wrapper">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8 bg-form-register">

                <div class="ang-form">
                    <h4>Załóż Konto</h4>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="reg_zwykle" value="konto_zwykle" name="customRadioInline1" class="custom-control-input" checked="checked">
                        <label class="custom-control-label" for="reg_zwykle">Zwykłe</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="reg_firmowe" value="konto_firmowe" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="reg_firmowe">Firmowe<p>
                            <small>Konto dla firm oraz osób prowadzących działalność gospodarczą.</small></label>
                    </div>
                </div>

                <div id="form">
	                <?php $register->registerSearch();?>
                    <form method="post" id="form-register-usually">

                        <!-- Form -->
                        <div class="form-group input-register">
                            <input class="form-control" type="email" name="email" id="email-input" placeholder="E-mail" required="required">
                        </div>
                        <div class="form-group input-register input-password-register">
                            <input class="form-control" type="password" name="password" placeholder="Hasło" id="password" required="required">
                            <i id="eye-icon" class="eye-toggle icon-eye-off align-middle"></i>
                        </div>
                        <p class="pass-text">
                            <small>Hasło powinno zawierać: jedną wielką literę, jedną małą literę, jedną cyfrę, od 8 do 16 znaków. </small>
                        </p>
                        <!-- Date Input Grup -->
                        <div class="date-form-birth">
                            <div class="form-group">
                                <input type="number" class="form-control"  name="day_birth" placeholder="Dzień" required="required">
                                <select name="month_birth" class="form-control" required="required">
                                    <option value="styczen">Styczeń</option>
                                    <option value="luty">Luty</option>
                                    <option value="marzec">Marzec</option>
                                    <option value="kwiecien">Kwiecień</option>
                                    <option value="maj">Maj</option>
                                    <option value="czerwiec">Czerwiec</option>
                                    <option value="lipiec">Lipiec</option>
                                    <option value="sierpien">Sierpień</option>
                                    <option value="wrzesien">Wrzesień</option>
                                    <option value="pazdziernik">Pazdziernik</option>
                                    <option value="listopad">Listopad</option>
                                    <option value="grudzien">Grudzień</option>
                                </select>
                                <input  type="number" name="year_birth" class="form-control" placeholder="Rok" required="required">
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <!-- Regulaminy Checkbox -->
                        <div id="regulaminy-checkbox">
                            <div class="checkbox-regulamin">
                                <input id="regAccept" class="m-choice" type="checkbox" name="regulamin_accept" value="accept" required="required">
                                <label for="regAccept" class="custom-control-label" >Oświadczam, że znam i akceptuję postanowienia
                                    <a href="#">Regulaminu Allegro. </a></label>
                            </div>
                            <div class="checkbox-regulamin">
                                <input  id="hanAccept" class="m-choice" type="checkbox" name="postanowienia_accept" value="accept">
                                <label for="hanAccept" class="custom-control-label" >Chcę otrzymywać informacje handlowe od Allegro.pl sp. z o.o. na podany adres e-mail (opcjonalnie)
                                    <a href="#">Rozwiń</a></label>
                            </div>
                            <div class="checkbox-regulamin">
                                <input id="infoAccept" class="m-choice" type="checkbox" name="handlowe_info" value="accept">
                                <label for="infoAccept" class="custom-control-label" >Chcę otrzymywać informacje handlowe firm współpracujących z Allegro.pl sp. z o.o. na podany adres e-mail (opcjonalnie)
                                    <a href="#">Rozwiń</a></label>
                            </div>
                        </div>

                        <div class="form-group text-right login_submit">
                            <input type="submit" class="register_submit align-right" name="reg_simple" id="register_simple" value="ZALÓŻ KONTO">
                        </div>


                    </form>
                </div>

            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>

<?php include_once PUBLIC_PATH . 'footer-empty.php'; ?>