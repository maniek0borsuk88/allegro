<?php include_once PUBLIC_PATH . 'header-empty.php'; ?>

<section id="login-section">
    <div class="body-login">
        <div class="login-wrapper container-login">
            <div class="row row-login">
                <div class="col-12 col-sm-6">
                    <div class="login-div">
                        <h2>Zaloguj się</h2>
                        <?php  $login = new \allegro\model\Login();?>
                        <div class="form-login text-right">
                            <form method="post" class="login-form">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control"  placeholder="Login lub e-mail">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Hasło">
                                </div>
                                <div class="haslo-przypomnij">
                                    <a href="#">nie pamiętasz hasła?</a>
                                </div>
                                <div class="login_submit">
                                    <input type="submit" name="submit_login" value="ZALOGUJ SIĘ">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="login-register-div">
                        <div class="link-register">
                            <p>Nie masz konta?<a href="#"><small> ZAREJESTRUJ SIĘ</small>   </a></p>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-sm-6">
                    <div class="banner-reklamowy img-fluid">
                        <a href="#">
                            <img class="img-fluid" src="<?php echo IMG_PATH . 'img/img5.jpg'; ?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<scetion id="strefa-login">
    <div class="login-wrapper">
        <div class="row">
            <div class="col-12 col-sm-4">
                <div class="card-login text-center">
                    <img src="<?php echo IMG_PATH . 'img/login1.png'?>" alt="">
                    <h4>Najlepszy wybór!</h4>
                    <p>95 milionów produktów w jednym miejscu!</p>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="card-login text-center">
                    <img src="<?php echo IMG_PATH . 'img/login2.png'?>" alt="">
                    <h4>Najlepsze ceny!</h4>
                    <p>100tyś. sklepów konkuruje o Twoja uwagę.</p>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="card-login text-center">
                    <img src="<?php echo IMG_PATH . 'img/login3.png'?>" alt="">
                    <h4>Zawsze bezpiecznie!</h4>
                    <p>Zwroty zakupów i ochrona kupującego dla kazdego</p>
                </div>
            </div>
        </div>
    </div>
</scetion>

<?php include_once PUBLIC_PATH . 'footer.php'; ?>