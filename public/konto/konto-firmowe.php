<?php include_once PUBLIC_PATH . 'header-empty.php'; ?>

<div id="register">
    <div class="login-wrapper">
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8 bg-form-register">

                <div class="ang-form">
                    <h4>Załóż Konto</h4>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="reg_zwykle" value="konto_zwykle" name="customRadioInline1" class="custom-control-input" required="required">
                        <label class="custom-control-label" for="reg_zwykle">Zwykłe</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="reg_firmowe" value="konto_firmowe" name="customRadioInline1" class="custom-control-input" checked="checked" required="required">
                        <label class="custom-control-label" for="reg_firmowe">Firmowe<p>
                                <small>Konto dla firm oraz osób prowadzących działalność gospodarczą.</small></label>
                    </div>
                </div>

                <div id="form">
                    <form method="post">
	                    <?php $register->registerSearch();?>
                    <div id="firma-1">
                            <div class="form-group input-register">
                                <input class="form-control" type="email" name="email" placeholder="E-mail" required="required">
                                <small><span>Na ten adres będziesz otrzymywać wszystkie wiadomości od Allegro.</span>
                                </small>
                            </div>

                            <div class="form-group input-register">
                                <input class="form-control" type="text" name="login" placeholder="Login"  required="required">
                                <small><span>Login będzie widoczny dla kupujących. Nie będziesz mógł go później zmienić. </span>
                                </small>
                            </div>

                            <div class="form-group input-register input-password-register">
                                <input class="form-control" type="password" name="password" placeholder="Hasło" id="password" required="required">
                                <i id="eye-icon" class="eye-toggle icon-eye-off align-middle"></i>
                            </div>
                            <p class="pass-text">
                                <small>Hasło powinno zawierać: jedną wielką literę, jedną małą literę, jedną cyfrę, od 8 do 16 znaków. </small>
                            </p>

                            <div class="form-group input-register">
                                <input class="form-control  float-left" type="text" name="telephone" placeholder="Telefon"  required="required">
                                <span id="tel-tol" class="btn badge badge-pill badge-info float-left m-2">i</span>
                                <span id="tel-info">
                                    <small><strong>
                                    Telefon musi być w formacie:
                                    500500500 lub 297414788 <br>
                                    500-500-500 lub 500 500 500 <br>
                                    29-74-000-00 lub 29 00 000 00
                                    </strong></small>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div id="firma-2">
                            <h6>Informacje o firmie</h6>

                            <div class="form-group input-register">
                                <input class="form-control" type="text" name="country" placeholder="Kraj"  required="required">
                            </div>

                            <div class="form-group input-register">
                                <input class="form-control" type="text" name="info_firma" placeholder="Informacje o firmie"  required="required">
                            </div>

                            <div class="form-group input-register">
                                <input class="form-control float-left" type="text" name="nip" placeholder="NIP" required="required">
                                <span id="nip-tol" class="btn badge badge-pill badge-info float-left m-2">i</span>
                                <span id="nip-info">
                                    <small><strong>
                                    Format nip:
                                    3330002244 <br>
                                    333-000-22-44 <br>
                                    333 000 22 44
                                    </strong></small>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>


                       <div id="firma-3">
                           <h6>Adres firmy</h6>

                           <div class="form-group input-register">
                               <input class="form-control adres-css" type="text" name="address" placeholder="Adres"  required="required">
                           </div>

                           <div class="form-group input-register" >
                               <input class="form-control kod-css float-left" type="text" name="kod" placeholder="Kod pocztowy"  required="required">
                               <span id="kod-tol" class="btn badge badge-pill badge-info float-left">i</span>
                               <span id="kod-info">
                                    <small><strong>
                                    Format kodu pocztowego:
                                    00-000
                                    </strong></small>
                                </span>


                               <input class="form-control miasto-css" type="text" name="miasto" placeholder="Miejscowość"  required="required">
                           </div>
                           <div class="clearfix"></div>
                       </div>


                        <!-- Regulaminy Checkbox -->
                        <div id="regulaminy-checkbox">
                            <div class="checkbox-regulamin">
                                <input id="regAccept" class="m-choice" type="checkbox" name="regulamin_accept" value="accept"  required="required">
                                <label for="regAccept" class="custom-control-label" >Oświadczam, że znam i akceptuję postanowienia
                                    <a href="#">Regulaminu Allegro. </a></label>
                            </div>
                            <div class="checkbox-regulamin">
                                <input  id="hanAccept" class="m-choice" type="checkbox" name="postanowienia_accept" value="accept">
                                <label for="hanAccept" class="custom-control-label" >Chcę otrzymywać informacje handlowe od Allegro.pl sp. z o.o. na podany adres e-mail (opcjonalnie)
                                    <a href="#">Rozwiń</a></label>
                            </div>
                            <div class="checkbox-regulamin">
                                <input id="infoAccept" class="m-choice" type="checkbox" name="handlowe_info" value="accept">
                                <label for="infoAccept" class="custom-control-label" >Chcę otrzymywać informacje handlowe firm współpracujących z Allegro.pl sp. z o.o. na podany adres e-mail (opcjonalnie)
                                    <a href="#">Rozwiń</a></label>
                            </div>
                        </div>

                        <div class="form-group text-right login_submit">
                            <input type="submit" class="register_submit align-right" name="reg_firm" value="ZALÓŻ KONTO">
                        </div>


                    </form>
                </div>

            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>

<?php include_once PUBLIC_PATH . 'footer-empty.php'; ?>






