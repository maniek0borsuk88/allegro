<!doctype html>
<html lang="pl" ng-app="APP">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Allegro</title>
	<link rel="stylesheet" href="/allegro/public/css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="/allegro/public/css/fontello/css/fontello.css" type="text/css" media="all">
    <link rel="stylesheet" href="/allegro/public/style.css" type="text/css" media="all">
</head>
<body>
<div id="header">
	<header>

            <div class="main-wrapper">
                <nav class="navbar background-nav">
                    <div class="col-12 col-sm-2"><a href="\allegro\"><?php echo \allegro\assets\SvgAssets::logoAllegro(); ?></a>
                    </div>
                <div class="col-12 col-sm-7 search-form">
                    <div id="form-search">
                        <form class="form-search">
                            <div class="input-group">
                                <input type="text" class="form-control" aria-label="Text input with segmented dropdown button" placeholder="czego szukasz?">
                                <select class="custom-select" id="inputGroupSelect04">
                                    <option value="moto">Motoryzacja</option>
                                    <option value="moda">Moda</option>
                                    <option value="elektronika">Elektronika</option>
                                    <option value="sport">Sport</option>
                                    <option value="nieruchomosc">Nieruchomośći</option>
                                </select>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button">   Szukaj  </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-sm-3 my-allegro-css text-right">
                    <a type="submit" class="btn-my-allegro align-middle" data-toggle="collapse" href="#my-allegro" >Moje Allegro <span class="arrow-svg icon-angle-down"></span></a>
                    <div class="group-my-allegro collapse text-left" id="my-allegro">
                        <ul class="my-allegro">
                            <li><a href="\allegro\myaccount\mojeaukcje">Moje aukcje</a></li>
                            <li><a href="\allegro\myaccount\addproduct">Dodaj przedmiot</a></li>
                            <li><a href="\allegro\myaccount\kupione">Kupione</a></li>
                            <li><a href="\allegro\myaccount\ulubione">Ulubione</a></li>
                            <hr>
                            <li><a href="\allegro\myaccount\mojekonto">Moje konto</a></li>
                            <hr>
                            <li><a href="\allegro\myaccount\logOut">Wyloguj się</a></li>
                        </ul>
                        <hr>
                        <div class="box-login text-center">
                            <div class="register">
                                <a href="\allegro\konto\register">ZALÓŻ KONTO</a>
                            </div>
                            <div class="login">
                                <a href="\allegro\konto\login">ZALOGUJ SIĘ</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
        </nav>
            </div>

		<nav id="nav-category">
			<ul class="nav justify-content-center">
				<li class="nav-item nav-category-li">
					<a class="nav-link active" href="\allegro\">Home</a>
				</li>
                <li class="nav-item nav-category-li">
					<a class="nav-link active" href="\allegro\dzial\moto">Motoryzacja</a>
				</li>
				<li class="nav-item nav-category-li">
					<a class="nav-link" href="\allegro\dzial\electro">Elektronika</a>
				</li>
				<li class="nav-item nav-category-li">
					<a class="nav-link" href="\allegro\dzial\sport">Sport</a>
				</li>
				<li class="nav-item nav-category-li">
					<a class="nav-link" href="\allegro\dzial\moda">Moda</a>
				</li>
				<li class="nav-item nav-category-li">
					<a class="nav-link" href="\allegro\dzial\nieruchomosc">Nieruchomośći</a>
				</li>
			</ul>
		</nav>
	</header>
</div>