
<footer>
    <!-- Footer -->
    <div id="footer">


    <div id="footer2">

        <div class="login-wrapper">
            <hr>
            <div class="row">
                <!-- Allegro -->
                <div class="col-12 col-sm-3 footer">
                    <h5>Allegro</h5>
                    <ul class="ul-footer">
                        <li><a href="#">O nas</a></li>
                        <li><a href="#">Dla kupujących</a></li>
                        <li><a href="#">Dla sprzedjących</a></li>
                        <li><a href="#">Praca</a></li>
                        <li><a href="#">Reklama</a></li>
                    </ul>
                </div>

                <!-- Serwisy -->
                <div class="col-12 col-sm-3  footer">
                    <h5>Serwisy</h5>
                    <ul class="ul-footer">
                        <li><a href="#">Strefa Marek</a></li>
                        <li><a href="#">Artykuły</a></li>
                        <li><a href="#">Karty Allegro</a></li>
                        <li><a href="#">Monety Allegro</a></li>
                        <li><a href="#">Archiwum</a></li>
                    </ul>
                </div>

                <!-- Centrum Pomocy -->
                <div class="col-12 col-sm-3 footer">
                    <h5>Centrum Pomocy</h5>
                    <ul class="ul-footer">
                        <li><a href="#">Pomoc</a></li>
                        <li><a href="#">Polityka plików Cookies</a></li>
                        <li><a href="#">Regulamin</a></li>
                        <li><a href="#">Dopasowanie Reklam</a></li>
                        <li><a href="#">Mapa strony</a></li>
                    </ul>
                </div>

                <!-- Okazje -->
                <div class="col-12 col-sm-3 footer">
                    <h5>Wyjątkowe Okazje</h5>
                    <ul class="ul-footer">
                        <li><a href="#">Okazje</a></li>
                        <li><a href="#">Najnowsze rabaty</a></li>
                        <li><a href="#">Allegro Smart</a></li>
                        <li><a href="#">Monety Allegro</a></li>
                        <li><a href="#">Prezenty</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="copyright"  class="footer-empty">
        <div class="login-wrapper">
            <div class="row">
                <div class="col-12 col-sm-6 text-left">
                    <p class="copy-paragraph">Korzystanie z serwisu oznacza akceptacje regulaminu <a href="#">regulamin</a></p>
                </div>
                <div class="col-12 col-sm-6 text-right">
                    <a href="\allegro\"><?php echo \allegro\assets\SvgAssets::logoAllegro(); ?></a>
                </div>
            </div>

        </div>
    </div>
</footer>






<script src="/allegro/public/js/jquery.js"></script>
<script src="/allegro/public/js/angular.js" type="text/javascript"></script>
<script src="/allegro/public/js/bootstrap.min.js"></script>
<script src="/allegro/public/js/bootstrap.bundle.min.js"></script>
<script src="/allegro/public/js/main.js" type="text/javascript"></script>
	</body>
</html>