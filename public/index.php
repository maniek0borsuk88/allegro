<?php include_once 'header.php'; ?>
<div class="body">
    <section id="polecane">
        <div class="main-wrapper bg-container-main">
            <div class="row polecane-row">
                <div class="col-12 col-sm-9">

                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">
                                </li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-item active">
                                <a href="#">
                                    <img class="d-block w-100" src="<?php echo IMG_PATH . 'img/img1.jpg'; ?>" alt="First slide">
                                </a>
                            </div>
                            <div class="carousel-item">
                                <a href="#">
                                    <img class="d-block w-100" src="<?php echo IMG_PATH . 'img/img2.jpg'; ?>" alt="Second slide">
                                </a>
                            </div>
                            <div class="carousel-item">
                                <a href="#">
                                    <img class="d-block w-100" src="<?php echo IMG_PATH . 'img/img3.jpg'; ?>" alt="Third slide">
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-sm-3">
                    <div class="card-banner">
                        <a href="#">
                            <img class="card-img-top" src="<?php echo IMG_PATH . 'img/img4.jpg'; ?>" alt="Card image cap">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="polecane2">
        <div class="main-wrapper">
            <div class="row card-allegro">

                <div class="polecane2-box col-12 col-sm-4">
                    <div class="jumbotron">
                        <h4 class="text-center">Okazje kończą sie za: <?php echo date('H:i:s'); ?></h4>
                        <div class="box-column-vertical">
                            <div class="card">
                                <img class="card-img-top" src="<?php echo IMG_PATH . 'img/product1.jpg'; ?>" alt="Card image cap">
                                <div class="card-body">
                                    <h4 class="price-main">2,99 zł</h4>
                                    <p><a href="#">Ciastka z Żurawiną...</a></p>
                                    <br>
                                    <p><small>36 osób kupiło</small></p>
                                    <a href="#">Ciastka z żurawiną<span class="badge badge-secondary">New</span></a>
                                    <s><p><small>3,99</small></p></s>
                                </div>
                            </div>
                        </div>
                        <div class="box-column-vertical">
                            <div class="card">
                                <img class="card-img-top" src="<?php echo IMG_PATH . 'img/product2.jpg'; ?>" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                </div>
                                <div class="card-footer">
                                    <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="jumbotron">
                        <h4>Okazje kończą sie za: <?php echo date('H:i:s'); ?></h4>

                        <div class="box-column-horizontal">
                            <div class="card">
                                <img class="card-img-top" src="..." alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                </div>
                                <div class="card-footer">
                                    <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                            </div>
                        </div>

                        <div class="box-column-horizontal">
                            <div class="card">
                                <img class="card-img-top" src="..." alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                </div>
                                <div class="card-footer">
                                    <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                            </div>
                        </div>

                        <div class="box-column-horizontal">
                            <div class="card">
                                <img class="card-img-top" src="..." alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                </div>
                                <div class="card-footer">
                                    <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="col-12 col-sm-4">

                    <div class="box-product">
                        <a href="#">
                            <img src="..." alt="">
                            <h4>Title</h4>
                        </a>
                    </div>
                    <div class="box-product">
                        <a href="#">
                            <img src="..." alt="">
                            <h4>Title</h4>
                        </a>
                    </div>
                    <div class="box-product">
                        <a href="#">
                            <img src="..." alt="">
                            <h4>Title</h4>
                        </a>
                    </div>
                    <div class="box-product">
                        <a href="#">
                            <img src="..." alt="">
                            <h4>Title</h4>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Section Polecane 3 -->
    <section id="polecane3">
        <div class="main-wrapper">
            <div class="row">
                <div class="col-6 col-sm-3 col-md-2">1</div>
                <div class="col-6 col-sm-3 col-md-2">2</div>
                <div class="col-6 col-sm-3 col-md-2">3</div>
                <div class="col-6 col-sm-3 col-md-2">4</div>
                <div class="col-6 col-sm-3 col-md-2">5</div>
                <div class="col-6 col-sm-3 col-md-2">6</div>
            </div>
        </div>
    </section>

    <!-- Section Info -->
    <section id="info">
        <div class="main-wrapper">
            <div class="card bg-dark text-white">
                <img class="card-img" src="..." alt="Card image">
                <div class="card-img-overlay">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    <p class="card-text">Last updated 3 mins ago</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Section Okazje -->
    <section id="okazje">
        <div class="col-4">
            <div id="carouselExampleControls2" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="box-column-vertical">
                            <p>Jestem Box 1</p>
                        </div>
                        <div class="box-column-vertical">
                            <p>Jestem Box 2</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="box-column-vertical">
                            <p>Jestem Box 3</p>
                        </div>
                        <div class="box-column-vertical">
                            <p>Jestem Box 4</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="box-column-vertical">
                            <p>Jestem Box 5</p>
                        </div>
                        <div class="box-column-vertical">
                            <p>Jestem Box 6</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>
</div>

<?php include_once 'footer.php'; ?>