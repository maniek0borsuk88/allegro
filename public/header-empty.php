<!doctype html>
<html lang="pl">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Allegro</title>
    <link rel="stylesheet" href="/allegro/public/css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="/allegro/public/css/fontello/css/fontello.css" type="text/css" media="all">
    <link rel="stylesheet" href="/allegro/public/style.css" type="text/css" media="all">
</head>
<body>
<?php
$register = new \allegro\model\Register(); ?>
<div id="header-empty">
	<header>

            <div class="login-wrapper">
                <nav class="navbar background-nav">
                    <div class="col-12 col-sm-2"><a href="\allegro\"><?php echo \allegro\assets\SvgAssets::logoAllegro(); ?></a>
                    </div>
                <div class="col-12 col-sm-7"></div>
                <div class="col-12 col-sm-3 my-allegro-css-login text-right">
                    <a type="submit" class="btn-my-allegro align-middle" data-toggle="collapse" href="#my-allegro" >Moje Allegro <span class="arrow-svg icon-angle-down"></span></a>
                    <div class="group-my-allegro collapse text-left" id="my-allegro">
                        <ul class="my-allegro">
                            <li><a href="\allegro\myaccount\mojeaukcje">Moje aukcje</a></li>
                            <li><a href="\allegro\myaccount\addproduct">Dodaj przedmiot</a></li>
                            <li><a href="\allegro\myaccount\kupione">Kupione</a></li>
                            <li><a href="\allegro\myaccount\ulubione">Ulubione</a></li>
                            <hr>
                            <li><a href="\allegro\myaccount\mojekonto">Moje konto</a></li>
                            <hr>
                            <li><a href="\allegro\myaccount\logOut">Wyloguj się</a></li>
                        </ul>
                        <hr>
                        <div class="box-login text-center">
                            <div class="register">
                                <a href="\allegro\konto\register">ZALÓŻ KONTO</a>
                            </div>
                            <div class="login">
                                <a href="\allegro\konto\login">ZALOGUJ SIĘ</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
        </nav>

            </div>


	</header>
</div>