$(document).ready(function () {

    $('#reg_zwykle').on('change', function () {
        window.location='/allegro/konto/register';
    });

    $('#reg_firmowe').on('change', function () {
        window.location='/allegro/konto/firmowe';

    });


    /**
    *  Function show and hide password for icon eye
    * */
    (function(){

        $('#eye-icon').on('click', function(){
            $('.eye-toggle').toggleClass('icon-eye-off icon-eye');
        });

        $('#eye-icon').on('click', function(){

            var $pass = $('#password');
            if($pass.attr('type')) {
                $pass.removeAttr('type');
            }else {
                $pass.attr('type','password');
            }

        });



    }());

    /**
     *
     * Function toltips mouseover and mouseout
     *
     * */
    function toltipsForm(idOne, idTwo) {

        $(idOne).mouseover(function () {
           $(idTwo).css('display','block');
        });

        $(idOne).mouseout(function () {
           $(idTwo).css('display','none');
        });

    }

    toltipsForm('#tel-tol','#tel-info');
    toltipsForm('#nip-tol','#nip-info');
    toltipsForm('#kod-tol','#kod-info');

});
